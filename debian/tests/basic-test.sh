#!/bin/sh
  
set -e

test_install_python3() {

    echo "Testing python3 package"
    for py in $(py3versions -r 2>/dev/null) ; do
        cd "$AUTOPKGTEST_TMP" ;
        echo "Testing with $py:" ;
        $py -c "import PyInstaller; print(PyInstaller)" ;
    done

}

test_generate_installer() {
  cat << EOF > pyinstaller-test-file.py
#!/usr/bin/env python3
print("Hello World!")
EOF
  pyinstaller pyinstaller-test-file.py
  ./dist/pyinstaller-test-file/pyinstaller-test-file | grep 'Hello World!'
}

###################################
# Main
###################################

for function in "$@"; do
        $function
done
