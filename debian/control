Source: pyinstaller
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Daniel Ruiz de Alegría <daniruiz@kali.org>,
           Devon Kearns <dookie@kali.org>,
           Sophie Brun <sophie@offensive-security.com>,
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-altgraph,
	       python3-lib2to3,
               python3-lxml,
               python3-mock,
               python3-psutil,
               python3-pytest,
               python3-setuptools,
               python3-tk,
               python3-wheel,
	       xmldiff
Standards-Version: 4.7.0
Homepage: http://www.pyinstaller.org/
Vcs-Git: https://gitlab.com/kalilinux/packages/pyinstaller.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/pyinstaller

Package: python3-pyinstaller
Architecture: all
Depends: python3-dev,
         python3-pyinstaller-hooks-contrib,
	 python3-setuptools,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Recommends: ${python3:Recommends},
Provides: pyinstaller,
Replaces: pyinstaller,
Conflicts: pyinstaller (<< 3.1.1),
           python-pyinstaller,
Suggests: ${python:Suggests},
XB-Python-Egg-Name: PyInstaller
Description: Converts (packages) Python programs into stand-alone executables.
 PyInstaller is a program that converts (packages) Python programs into stand-
 alone executables, under Windows, Linux, Mac OS X, Solaris and AIX. Its main
 advantages over similar tools are that PyInstaller works with any version of
 Python since 2.3, it builds smaller executables thanks to transparent
 compression, it is fully multi-platform, and use the OS support to load the
 dynamic libraries, thus ensuring full compatibility.
 .
 The main goal of PyInstaller is to be compatible with 3rd-party packages out
 -of-the-box. This means that, with PyInstaller, all the required tricks to make
 external packages work are already integrated within PyInstaller itself so that
 there is no user intervention required. You'll never be required to look for
 tricks in wikis and apply custom modification to your files or your setup
 scripts. As an example, libraries like PyQt, Django or matplotlib are fully
 supported, without having to handle plugins or external data files manually.
